Ex: assemblage d'une voiture
 - Créér un chassis
 - Créer une carroserie
 - Créer un moteur
 - Mettre le pneu
 - Assembler le tous et tester le tous
 
 Ce qui nous ammène à 2 process :
  - Assemblage
  - Test si l'assemblage s'est bien effectué
 
Pareil sur git lab on a 2 process à faire pour assembler une voiture
 - build 
 - test
 
 Concretement allons voir tous cela :
 
 ```yaml
construire une voiture:
    script:
        - mkdir construire
        - cd construire
        - touch vehicule.txt
        # créons notre chassis
        - echo "Chassis" > vehicule.txt
        - echo "Carroserie" > vehicule.txt
        - echo "Moteur" > vehicule.txt
        - echo "Pneu" > vehicule.txt
    # look at the video
test la construction vehicule:
    script:
        # test permet de creer des expression conditionnels https://fr.wikipedia.org/wiki/Test_(Unix)
        - test -f construire/vehicule.txt  # vérifier si le fichier existe
        - cd construire
        - grep "Chassis" vehicule.txt
        - grep "Carroserie" vehicule.txt
        - grep "Moteur" vehicule.txt
        - grep "Pneu" vehicule.txt
```

Après cela il faut definir à gitlab les étapes à suivre.
Ce qu'on appele ```job``` dans le fichier gitlab on le decript dans `stage`

```yaml
stages:
    - construire
    - test
```

Les jobs sont par défault indépendant les unes des autre à moins d'un appel spécifique.
et n'échange aucun donnée entre eux. Néanmoins  
pbm qui se pose : 
 - Une fois le job exécuter : les fichier seront supprimés
 - Le job suivant n'as aucune édee de l'interieur du référentiel
 pour lié les deux on a besoin d'un `artéfact`
 
 ```yaml
artefacts:
    paths:
      - construire
```