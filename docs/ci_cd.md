CI:
- Pratique pour l'intégration des codes 
- Assure la compilation des projets
- Assure que ca passe les tests, les configurations, la standarisation des codes

Explain :

    - Travail en équipe => beaucoup de modification de code
    - Verifier si les nouveau code intégré fonctionne très bien
    - Verifier si un paquet casse la compilation
    - Verifier sous different niveau de test
    - Intégrer le travail que nous faisons tous le temp

Step CI : 
```yaml
 - build #construction
 - test quality code # test de qualité de code
 - Tests  # test unitaire et fonctionnel
 - package # un  livrable à déployer
```
    
CD :
 - A partir du paquet livré de CI et l'idée c'est de livrer et construire un paquet tous le temp 
 et les installer dans un environnement et seulement en utilisant une intervention manuelle.
Avantages :

- Erreur sont détecter dans le process de développement
- Réduire les problème d'intégration
- Chaque dev peut travailler rapidement
 